<?php

//$_SERVER["DOCUMENT_ROOT"] = "../../..";
include('../master.inc.php');
require_once '../vendor/autoload.php';

class EncryptionTest extends PHPUnit_Framework_TestCase {
    var $encryption, $string_to_encrypt;
    function __construct() {
        parent::__construct();
    }

    public function setUp() {
        $this->string_to_encrypt = "The quick brown fox jumps over the lazy dog!";

        $this->encryption = new Encryption();
    }

    public function tearDown() {
        
    }

    public function testEncryption() {

        $encrypted = $this->encryption->encrypt($this->string_to_encrypt);
        $decrypted = $this->encryption->decrypt($encrypted);

        $this->assertEquals($this->string_to_encrypt, $decrypted);
    }
}