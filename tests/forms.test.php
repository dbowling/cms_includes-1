<?php

$_SERVER["DOCUMENT_ROOT"] = "../../..";
include('../master.inc.php');
require_once 'PHPUnit/Autoload.php';

class FormsTest extends PHPUnit_Framework_TestCase {

    var $form;
    var $postvars;
    var $fields;

    function __construct() {
        parent::__construct();
    }

    public function setUp() {
        $_SERVER["HTTP_REFERER"] = 'http://www.umt.edu/';
        $_SERVER['REMOTE_ADDR'] = "127.0.0.1";
        $_SERVER['HTTP_HOST'] = "umt.edu";
        $_SERVER['REQUEST_URI'] = "info.php";
        $_SERVER['HTTP_USER_AGENT'] = "Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))";

        $this->postvars = array(
            "test_question" => "test response",
            "Your_Email" => "nick.shontz@mso.umt.edu",
            "Join_the_mailinglist" => "Yes"
        );

        $this->fields = array(
            array(
                "type" => "text",
                "name" => "test question",
                "class" => "",
                "help_text" => "What your parents named you",
                "required" => true),
            array(
                "type" => "text",
                "name" => "Your Email",
                "class" => "",
                "required" => false),
            array(
                "type" => "checkbox",
                "name" => "Join the mailinglist",
                "class" => "",
                "help_text" => "We will never share your information",
                "required" => false)
        );
    }

    public function tearDown() {
        
    }

    public function testRefererCheck() {
        $default_config = array("acceptable_referers" => array('www.umt.edu'));
        $this->form = new Forms($default_config);
        $this->assertTrue($this->form->referer_check());

        $_SERVER["HTTP_REFERER"] = 'http://life.umt.edu/';
        $default_config = array("acceptable_referers" => array('www.umt.edu'));
        $this->form = new Forms($default_config);
        $this->assertFalse($this->form->referer_check());
    }

    /**
     * This should send a real email 
     */
    public function testSendEmail() {
        $default_config = array("form_id" => 'testing_form_id_' . __FUNCTION__);
        $this->form = new Forms($default_config);
        $to = "nick.shontz@umontana.edu";
        $this->assertTrue($this->form->send_email($to, $this->postvars));
    }

    public function testDisplayEmailForm() {
        $default_config = array("form_id" => 'testing_form_id_' . __FUNCTION__);

        $this->form = new Forms($default_config);
        $to = "nick.shontz@umontana.edu";

        ob_start();
        $this->form->display_email_form($this->fields, $to);
        $output = ob_get_contents();
        ob_end_clean();

        $this->AssertContains("type='submit'", $output);
        $this->AssertContains("id='testing_form_id_" . __FUNCTION__ . "'", $output);
    }

    public function testValidatingEmailForm() {
        $default_config = array("form_id" => 'testing_form_id_' . __FUNCTION__, "thanks" => "testing thanks text");

        $this->form = new Forms($default_config);
        $to = "nick.shontz@umontana.edu";
        $this->postvars['send'] = true;
        $this->fields[] = array(
            "type" => "text",
            "name" => "Required Field",
            "class" => "",
            "help_text" => "We will never share your information",
            "required" => true);
        ob_start();
        $this->form->display_email_form($this->fields, $to, $this->postvars);
        $output = ob_get_contents();
        ob_end_clean();

        $this->AssertContains("The following fields are required", $output);
    }

    /**
     * This should send a real email 
     */
    public function testSendingEmailForm() { 
       $default_config = array("form_id" => 'testing_form_id_' . __FUNCTION__, "thanks" => "testing thanks text");

        $this->form = new Forms($default_config);
        $to = "nick.shontz@umontana.edu";
        $this->postvars['send'] = true;
        ob_start();
        $this->form->display_email_form($this->fields, $to, $this->postvars);
        $output = ob_get_contents();
        ob_end_clean();

        $this->AssertContains("testing thanks text", $output);
    }

}