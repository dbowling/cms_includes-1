<?php

$_SERVER["DOCUMENT_ROOT"] = "../../..";
include('../master.inc.php');
require_once 'PHPUnit/Autoload.php';

class WeatherTest extends PHPUnit_Framework_TestCase {



    function __construct() {
        parent::__construct();
    }

    public function setUp() {
    }

    public function tearDown() {
        
    }

    public function testSendingEmailForm() { 
       $default_config = array("form_id" => 'testing_form_id_' . __FUNCTION__, "thanks" => "testing thanks text");

        $this->form = new Forms($default_config);
        $to = "nick.shontz@umontana.edu";
        $this->postvars['send'] = true;
        ob_start();
        $this->form->display_email_form($this->fields, $to, $this->postvars);
        $output = ob_get_contents();
        ob_end_clean();

        $this->AssertContains("testing thanks text", $output);
    }

}