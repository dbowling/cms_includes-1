<?php

if (!class_exists("cart") && !class_exists("item") && !class_exists("payment")) {

	class cart
	{

		var $items, $total;

		function __construct($name = "cart", $persist = true)
		{

			$this->cart_name = $name;
			$this->persist = $persist;
			$this->items = array();
			$this->total = 0;
			if ($this->persist == false) {
				unset($_SESSION[$this->cart_name]);
			} else if (isset($_SESSION[$this->cart_name])) {
				$this->rebuild(unserialize($_SESSION[$this->cart_name]));
			}
		}

		private function rebuild($cart)
		{
			$this->items = $cart->items;
			$this->update_total();
		}

		function get_item_names()
		{
			$names = array();
			foreach ($this->items as $item) {
				$names[] = $item->name;
			}
			return $names;
		}

		function add($item)
		{
			$requested_item = $this->get_item($item->id);
			if ($requested_item != null) {
				$this->items[$requested_item->unique_id]->increase_quantity();
			} else {
				$this->items[$item->unique_id] = $item;
			}
			$this->save();
		}

		private function get_item($item_id)
		{
			$requested_item = null;
			foreach ($this->items as $item) {
				if ($item->id == $item_id) {
					$requested_item = $item;
				}
			}
			return $requested_item;
		}

		function remove($id)
		{
			unset($this->items[$id]);
			$this->save();
		}

		function reset()
		{
			foreach ($this->items as $item) {
				unset($item);
			}
			$this->save();
			unset($_SESSION[$this->cart_name]);
			$this->rebuild(new cart());
		}

		private function update_total()
		{
			$total = 0;
			foreach ($this->items as $item) {
				$total = $total + ($item->amount * $item->quantity);
			}
			$this->total = $total;
		}

		function save()
		{
			$this->update_total();
			if ($this->persist == true) {
				$_SESSION[$this->cart_name] = serialize($this);
			}
		}

	}

	class item
	{

		var $name;

		function __construct($name, $amount)
		{
			$this->id = md5($name . $amount);
			$this->unique_id = md5($name . $amount . mktime()) . uniqid("", true);
			$this->name = $name;
			$this->amount = $amount;
			$this->quantity = 1;
		}

		public function increase_quantity($increase = 1)
		{

			$this->quantity = $this->quantity + $increase;
		}

		public function set_quantity($quantity)
		{
			$this->quantity = $quantity;
		}

	}

	class payment
	{

		var $variables, $host, $error, $use_cart, $cart;

		function __construct($merchant_id, $reg_key, $reference_id = "", $use_cart = false, $return_url = "", $use_confirm = "")
		{

			$this->encryption = new Encryption();
			$this->utility = new utility();
			$this->config = (object)array("db" => "mysql",
				"db_config" => array(
					"user" => "root",
					"pass" => "itopass",
					"host" => "localhost",
					"port" => 3306,
					"database" => "forms"));

			$this->use_cart = $use_cart;

			if ($this->use_cart) {
				$this->cart = new cart();
			}
			$this->error->status = false;
			$this->host = "https://webservices.primerchants.com/billing/TransactionCentral/EnterTransaction.asp?";
			$this->variables->MerchantID = $merchant_id;
			$this->variables->RegKey = $reg_key;
			$this->variables->ConfirmPage = (strtolower($use_confirm) == 'y' ? 'Y' : 'N');
			$this->variables->RURL = (!empty($return_url) ? $return_url : (((isset($_SERVER['HTTP']) && $_SERVER['HTTP'] == "on") ? "https://" : "http://")) . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . "?action=" . $this->encryption->encrypt("thanks"));
			$this->variables->RefID = (!empty($reference_id) ? $reference_id : uniqid());
			$this->styles_included = false;
		}

		private function validate_amount($input)
		{
			$amount = ltrim($input, '$ ');

			if (!is_numeric($amount)) {
				$this->error->status = true;
				$this->error->message = "<div class='payment'><p class='error'>Incorrectly formatted amount, must be in the format of <em>0.00</em></p></div>";
			} else {
				$amount = number_format($amount, 2, ".", "");
			}
			return $amount;
		}

		private function include_styles($return = false)
		{
			$output = "";
			if ($this->styles_included == false) {
				$output = "<link href='" . $this->utility->site_url() . "/_common/includes/resources/payment.css' rel='stylesheet' type='text/css' />\n";
				$this->styles_included = true;
			}
			if ($return) {
				return $output;
			} else {
				print($output);
			}
		}

		function payment_button($item_name, $amount, $options = array(), $template = "", $is_cart = false, $return_string = false)
		{

			$output = $this->include_styles(true);

			if (is_object($item_name)) {
				$cart = $item_name;
				$item_name = implode(", ", $cart->get_item_names());
				$amount = $cart->total;
			}
			$default_options = array(
				"wrapper_class" => "",
				"anchor_class" => "",
				"link" => "?action=" . $this->encryption->encrypt("display_form") . ($is_cart ? "&c=" . $this->encryption->encrypt("true") : "&a=" . $this->encryption->encrypt($this->validate_amount($amount)) . "&n=" . $this->encryption->encrypt($item_name)),
				"text" => "Buy Now",
				"message_class" => "message",
				"cart_class" => "",
				"cart_link" => "?action=" . $this->encryption->encrypt("addToCart") . "&n=" . $this->encryption->encrypt($item_name) . "&a=" . $this->encryption->encrypt($this->validate_amount($amount)),
				"cart_text" => "add to cart");

			$options = array_merge($default_options, $options);

			if (empty($template)) {
				$template .= "";
				if (isset($_SESSION['message']) && !empty($_SESSION['message'])) {
					$template .= "\n<p class='{message_class}'>" . $_SESSION['message'] . "</p>";
					unset($_SESSION['message']);
				}
				$template .= "\n<a class='{anchor_class}' href='{link}'>{text}</a>";
				if ($this->use_cart && !$is_cart) {
					$template .= "\n<a class='{cart_class}' href='{cart_link}'>{cart_text}</a>";
				}
			}

			$output .= $this->parse_template($template, $options);


			if (isset($this->error->message) && !empty($this->error->message)) {
				$output .= $this->error->message;
			}

			if ($return_string) {
				return $output;
			} else {
				print($output);
			}
		}

		private function parse_template($template, $options)
		{
			$template_variables = array();
			foreach ($options as $key => $value) {
				$template_variables['{' . $key . '}'] = $value;
			}
			return str_replace(array_keys($template_variables), $template_variables, $template);
		}

		function show_cart($options = array())
		{

			if ($this->use_cart) {
				$default_options = array(
					"cart_title" => "Shopping Cart",
					"currency_symbol" => "$",
					"cart_class" => "cart",
					"item_header" => "Item",
					"amount_header" => "Amount",
					"quantity_header" => "Quantity",
					"total_header" => "Amount",
					"delete_text" => "delete",
					"update_quantity_text" => "Update",
					"item_count" => "",
					"no_items_text" => "You have no items in your cart",
					"item_count_left" => "(",
					"item_count_right" => ")",
					"max_quantity" => 10
				);
				$options = array_merge($default_options, $options);

				$item_list = array();
				$names = array();
				foreach ($this->cart->items as $id => $item) {
					$quantity = $item->quantity . " <a href='?action=" . $this->encryption->encrypt("edit_item_quantity") . "&i=" . $this->encryption->encrypt($id) . "'>{update_quantity_text}</a>";
					$quantity_options = array();
					for ($i = 0; $i < $options['max_quantity']; $i++) {
						$quantity_options[] = "<option " . ($i + 1 == $item->quantity ? "selected='selected'" : "") . " value='" . ($i + 1) . "'>" . ($i + 1) . "</options>";
					}
					$quantity = "<form action='?action=" . $this->encryption->encrypt("update_item_quantity") . "' method='post'><label for='quantity'>Quantity</label><select name='quantity'>" . implode("\n", $quantity_options) . "</select><input type='submit' value='Update' class='submit'/><input type='hidden' name='id' value='" . $item->unique_id . "'/></form>";

					$item_list[] = "\n\t<tr>\n\t\t<td>" . $item->name . "</td><td>" . $quantity . "</td><td>{currency_symbol}" . ($item->amount * $item->quantity) . "</td>\n\t\t<td><a href='?action=" . $this->encryption->encrypt("remove_cart_item") . "&i=" . $this->encryption->encrypt($id) . "'>{delete_text}</a></td>\n\t</tr>";
					$names[] = $item->name;
				}
				$cart_body = "";

				if (count($this->cart->items) > 0) {
					$options["item_count"] = "{item_count_left}" . count($this->cart->items) . "{item_count_right}";
					$cart_body .= "<table><thead><tr><th>{item_header}</th><th>{quantity_header}</th><th>{amount_header}</th></tr></thead><tbody>\n" .
						implode($item_list) . "\n</tbody><tfoot><tr><th colspan='2'>{total_header}</th><th>{currency_symbol}" .
						$this->cart->total . "</th><th>" .
						$this->payment_button($this->cart, null, array("text" => "checkout"), "", true, true) .
						"</th></tr></tfoot></table>";
				} else {
					$cart_body .= "<p class='no_items'>{no_items_text}</p>";
				}

				$template = "\n<div class='payment'><div class='{cart_class}'><h2>{cart_title} " . $options["item_count"] . "</h2>" . $cart_body . "</div></div>";
				//print("<pre>" . print_r($this->cart, true) . "</pre>");
				print($this->parse_template($template, $options));
			}
		}

		private function persist_transaction_data($merchant_id, $trans_id, $trans_type, $ref_no, $auth, $avs_code, $cvv2_response, $notes)
		{
			$insert_id = null;
			if (class_exists('mysqli') && isset($this->config->db_config['user'])) {
				$mysqli = new mysqli($this->config->db_config['host'],
					$this->config->db_config['user'],
					$this->config->db_config['pass'],
					$this->config->db_config['database'],
					$this->config->db_config['port']);

				if ($mysqli->connect_error === null) {
					$sql = "INSERT INTO payments (`merchant_id`,`trans_id`,`trans_type`,`ref_no`,`auth`,`avs_code`,`cvv2_response`,`notes`)
                        values (\"" . $merchant_id . "\",\"" . $trans_id . "\",\"" . $trans_type . "\",\"" . $ref_no . "\",\"" . $auth . "\",\"" . $avs_code . "\",\"" . $cvv2_response . "\",\"" . $notes . "\");";
					$mysqli->query($sql);
					$insert_id = $mysqli->insert_id;
				} else {
					//error logging
				}
				$mysqli->close();
			}
			return $insert_id;
		}

		private function merge_template($template, $content, $return_string = false)
		{
			$keys = array_keys($content);
			$find = array();
			foreach ($keys as $key) {
				$find[] = "{" . $key . "}";
			}
			$replace = $content;
			$output = str_replace($find, $replace, $template);
			if ($return_string == true) {
				return $output;
			} else {
				print($output);
			}
		}

		private function display_cart_review($cart, $return_string = false)
		{
			$output = "<div  class='payment_review'><h2>Order Review</h2><table cellspacing='0'><thead><tr><th>Item</th><th>Quantity</th><th>Cost</th></tr></thead><tbody>";

			foreach ($cart->items as $item) {
				$output .= "<tr><td>" . $item->name . "</td><td>" . $item->quantity . "</td><td>$" . number_format($item->amount * $item->quantity, 2) . "</td></tr>";
			}

			$output .= "</tbody><tfoot><tr><th colspan='2'>Total</th><td>$" . number_format($cart->total, 2) . "</td></tfoot></table></div>";

			if ($return_string == true) {
				return $output;
			} else {
				print($output);
			}
		}

		function process($config, $fields, $to)
		{
			$default_config = array(
				"template_file" => null,
				"template" => "<!DOCTYPE html>\n<html><head><title>{title}</title></head><body>\n{content}\n</body>\n</html>",
				"purchase_complete" => "Purchase Complete",
				"action" => $this->utility->site_url(htmlentities($_SERVER['PHP_SELF']) . "?action=" . $this->encryption->encrypt("process_payment")),
			);

			$config = array_merge($default_config, $config);
			if (isset($_GET['action'])) {
				$Forms = new Forms($config, true);
				$action = $this->encryption->decrypt($this->utility->xss_protect($_GET['action']));
				$amount = (isset($_GET['a']) ? $this->validate_amount($this->utility->xss_protect($this->encryption->decrypt($_GET['a']))) : null);
				$item_id = (isset($_GET['i']) ? $this->utility->xss_protect($this->encryption->decrypt($_GET['i'])) : null);
				$item_name = (isset($_GET['n']) ? $this->utility->xss_protect($this->encryption->decrypt($_GET['n'])) : null);
				$is_cart = (isset($_GET['c']) && $this->utility->xss_protect($this->encryption->decrypt($_GET['c'])) == "true" ? true : false);
				if ($this->error->status == false && $action == "display_form") {
					if (!$is_cart) {
						$this->cart = new cart(md5("instant_cart" . microtime()));
						$this->cart->add(new item($item_name, $amount));
					}
					$fields[] = array(
						"type" => "hidden",
						"name" => "cart_json",
						"help_text" => "",
						"value" => json_encode($this->cart),
						"fieldset" => "",
						"options" => array(),
						"required" => false
					);

					$form = $Forms->display_email_form($fields, $to, $_POST, true);

					$template = (!empty($config['template_file']) ? file_get_contents($config['template_file']) : $config['template']);
					$content = $this->include_styles(true);
					$content .= $Forms->include_styles(true, true);
					$content .= $this->display_cart_review($this->cart, true);
					$content .= $form->html_output;

					$this->merge_template($template, array('content' => $content, 'title' => 'Payment'));

					exit;
				} else if ($this->error->status == false && $action == "process_payment") {

					$response = $Forms->display_email_form($fields, $to, $_POST, true);
					$cart = json_decode(html_entity_decode($response->data->cart_json));

					$this->variables->Amount = $cart->total;
					$this->variables->RefID = $response->db_insert_id;
					$output = "<!DOCTYPE html>\n<html><body>\n<form id='payment_form' action='" . $this->host . "' method='post'>";
					foreach ($this->variables as $key => $value) {
						$output .= "\n\t<input type='hidden' value='" . $value . "' name='" . $key . "'/>";
					}
					$output .= "\n</form><script type='text/javascript'>document.forms['payment_form'].submit();</script></body></html>";
					print($output);
					exit;
				} else if ($this->error->status == false && $action == "thanks") {
					$id = $this->persist_transaction_data($_POST['MerchantID'], $_POST['TransID'], $_POST['TransType'], $_POST['RefNo'], $_POST['Auth'], $_POST['AVSCode'], $_POST['CVV2ResponseMsg'], $_POST['Notes']);
					$this->cart->reset();
					if (is_numeric($_POST['Auth'])) {
						$_SESSION['message'] = $config->purchase_complete;
					} else {
						$_SESSION['message'] = end(explode("\\", $_POST['Notes']));
					}
					$this->utility->redirect($_SERVER['PHP_SELF']);
				} else if ($this->error->status == false && $action == "addToCart") {
					$this->cart->add(new item($item_name, $amount));
					$this->utility->redirect($_SERVER['PHP_SELF']);
				} else if ($this->error->status == false && $action == "remove_cart_item") {
					$this->cart->remove($item_id);
					$this->utility->redirect($_SERVER['PHP_SELF']);
				} else if ($this->error->status == false && $action == "update_item_quantity") {
					$this->cart->items[$_POST['id']]->set_quantity($_POST['quantity']);
					$this->cart->save();
					$this->utility->redirect($_SERVER['PHP_SELF']);
				}
			}
		}

	}
}