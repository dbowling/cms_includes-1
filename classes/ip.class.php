<?php
if (!class_exists('ip')) {

	class ip
	{
		/**
		 * IP address of the current user
		 *
		 * @var string
		 */
		var $ip_address = FALSE;

		// --------------------------------------------------------------------

		/**
		 * Fetch from array
		 *
		 * This is a helper function to retrieve values from global arrays
		 *
		 * @access    private
		 * @param    array
		 * @param    string
		 * @param    bool
		 * @return    string
		 */
		function _fetch_from_array(&$array, $index = '')
		{
			if (!isset($array[$index])) {
				return FALSE;
			}

			return $array[$index];
		}

		/**
		 * Fetch an item from the SERVER array
		 *
		 * @access    public
		 * @param    string
		 * @param    bool
		 * @return    string
		 */
		public function server($index = '')
		{
			return $this->_fetch_from_array($_SERVER, $index);
		}

		// --------------------------------------------------------------------

		public function on_local_network()
		{
			$on_local_network = false;
			$ip_address = $this->ip_address();
			$octets = explode(".", $ip_address);
			if ($octets[0] == "10") {
				$on_local_network = true;
			}
			return $on_local_network;
		}

		/**
		 * Fetch the IP Address
		 *
		 * @access    public
		 * @return    string
		 */
		public function ip_address()
		{
			if ($this->ip_address !== FALSE) {
				return $this->ip_address;
			}

			$proxy_ips = "10.10.7.197,10.10.7.195";
			if (!empty($proxy_ips) && !is_array($proxy_ips)) {
				$proxy_ips = explode(',', str_replace(' ', '', $proxy_ips));
			}

			$this->ip_address = $this->server('REMOTE_ADDR');

			if ($proxy_ips) {
				foreach (array('HTTP_X_FORWARDED_FOR', 'HTTP_CLIENT_IP', 'HTTP_X_CLIENT_IP', 'HTTP_X_CLUSTER_CLIENT_IP') as $header) {
					if (($spoof = $this->server($header)) !== NULL) {
						// Some proxies typically list the whole chain of IP
						// addresses through which the client has reached us.
						// e.g. client_ip, proxy_ip1, proxy_ip2, etc.
						sscanf($spoof, '%[^,]', $spoof);

						if (!$this->valid_ip($spoof)) {
							$spoof = NULL;
						} else {
							break;
						}
					}
				}

				if ($spoof) {
					for ($i = 0, $c = count($proxy_ips); $i < $c; $i++) {
						// Check if we have an IP address or a subnet
						if (strpos($proxy_ips[$i], '/') === FALSE) {
							// An IP address (and not a subnet) is specified.
							// We can compare right away.
							if ($proxy_ips[$i] === $this->ip_address) {
								$this->ip_address = $spoof;
								break;
							}

							continue;
						}

						// We have a subnet ... now the heavy lifting begins
						isset($separator) OR $separator = $this->valid_ip($this->ip_address, 'ipv6') ? ':' : '.';

						// If the proxy entry doesn't match the IP protocol - skip it
						if (strpos($proxy_ips[$i], $separator) === FALSE) {
							continue;
						}

						// Convert the REMOTE_ADDR IP address to binary, if needed
						if (!isset($ip, $sprintf)) {
							if ($separator === ':') {
								// Make sure we're have the "full" IPv6 format
								$ip = explode(':',
									str_replace('::',
										str_repeat(':', 9 - substr_count($this->ip_address, ':')),
										$this->ip_address
									)
								);

								for ($i = 0; $i < 8; $i++) {
									$ip[$i] = intval($ip[$i], 16);
								}

								$sprintf = '%016b%016b%016b%016b%016b%016b%016b%016b';
							} else {
								$ip = explode('.', $this->ip_address);
								$sprintf = '%08b%08b%08b%08b';
							}

							$ip = vsprintf($sprintf, $ip);
						}

						// Split the netmask length off the network address
						sscanf($proxy_ips[$i], '%[^/]/%d', $netaddr, $masklen);

						// Again, an IPv6 address is most likely in a compressed form
						if ($separator === ':') {
							$netaddr = explode(':', str_replace('::', str_repeat(':', 9 - substr_count($netaddr, ':')), $netaddr));
							for ($i = 0; $i < 8; $i++) {
								$netaddr[$i] = intval($netaddr[$i], 16);
							}
						} else {
							$netaddr = explode('.', $netaddr);
						}

						// Convert to binary and finally compare
						if (strncmp($ip, vsprintf($sprintf, $netaddr), $masklen) === 0) {
							$this->ip_address = $spoof;
							break;
						}
					}
				}
			}

			if (!$this->valid_ip($this->ip_address)) {
				return $this->ip_address = '0.0.0.0';
			}

			return $this->ip_address;
		}

		// --------------------------------------------------------------------

		/**
		 * Validate IP Address
		 *
		 * @access    public
		 * @param    string
		 * @param    string    ipv4 or ipv6
		 * @return    bool
		 */
		public function valid_ip($ip, $which = '')
		{
			$which = strtolower($which);

			// First check if filter_var is available
			if (is_callable('filter_var')) {
				switch ($which) {
					case 'ipv4':
						$flag = FILTER_FLAG_IPV4;
						break;
					case 'ipv6':
						$flag = FILTER_FLAG_IPV6;
						break;
					default:
						$flag = '';
						break;
				}

				return (bool)filter_var($ip, FILTER_VALIDATE_IP, $flag);
			}

			if ($which !== 'ipv6' && $which !== 'ipv4') {
				if (strpos($ip, ':') !== FALSE) {
					$which = 'ipv6';
				} elseif (strpos($ip, '.') !== FALSE) {
					$which = 'ipv4';
				} else {
					return FALSE;
				}
			}

			$func = '_valid_' . $which;
			return $this->$func($ip);
		}

		// --------------------------------------------------------------------

		/**
		 * Validate IPv4 Address
		 *
		 * Updated version suggested by Geert De Deckere
		 *
		 * @access    protected
		 * @param    string
		 * @return    bool
		 */
		protected function _valid_ipv4($ip)
		{
			$ip_segments = explode('.', $ip);

			// Always 4 segments needed
			if (count($ip_segments) !== 4) {
				return FALSE;
			}
			// IP can not start with 0
			if ($ip_segments[0][0] == '0') {
				return FALSE;
			}

			// Check each segment
			foreach ($ip_segments as $segment) {
				// IP segments must be digits and can not be
				// longer than 3 digits or greater then 255
				if ($segment == '' OR preg_match("/[^0-9]/", $segment) OR $segment > 255 OR strlen($segment) > 3) {
					return FALSE;
				}
			}

			return TRUE;
		}

		// --------------------------------------------------------------------

		/**
		 * Validate IPv6 Address
		 *
		 * @access    protected
		 * @param    string
		 * @return    bool
		 */
		protected function _valid_ipv6($str)
		{
			// 8 groups, separated by :
			// 0-ffff per group
			// one set of consecutive 0 groups can be collapsed to ::

			$groups = 8;
			$collapsed = FALSE;

			$chunks = array_filter(
				preg_split('/(:{1,2})/', $str, NULL, PREG_SPLIT_DELIM_CAPTURE)
			);

			// Rule out easy nonsense
			if (current($chunks) == ':' OR end($chunks) == ':') {
				return FALSE;
			}

			// PHP supports IPv4-mapped IPv6 addresses, so we'll expect those as well
			if (strpos(end($chunks), '.') !== FALSE) {
				$ipv4 = array_pop($chunks);

				if (!$this->_valid_ipv4($ipv4)) {
					return FALSE;
				}

				$groups--;
			}

			while ($seg = array_pop($chunks)) {
				if ($seg[0] == ':') {
					if (--$groups == 0) {
						return FALSE; // too many groups
					}

					if (strlen($seg) > 2) {
						return FALSE; // long separator
					}

					if ($seg == '::') {
						if ($collapsed) {
							return FALSE; // multiple collapsed
						}

						$collapsed = TRUE;
					}
				} elseif (preg_match("/[^0-9a-f]/i", $seg) OR strlen($seg) > 4) {
					return FALSE; // invalid segment
				}
			}

			return $collapsed OR $groups == 1;
		}
	}
}