<?php
/**
 ** Title: Display Current Weather
 ** Description: Allows you to show the current weather
 ** Parameters: A zip code
 **/

$weather = new Weather();

$weather->display_current();

$config = array("display"=>"horizontal");
$weather->display_current($config);